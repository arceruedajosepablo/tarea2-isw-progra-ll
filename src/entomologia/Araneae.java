/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Pablo08
 */
public class Araneae extends Entomologia{
    private String claAraneae;
    private boolean veneno;
    public Araneae(String pClase, String pReino, String pFilo, String pOrden, String pSubOrden, String pClaAraneae, boolean pVeneno) {
        super(pClase, pReino, pFilo, pOrden, pSubOrden);
        this.claAraneae = pClaAraneae;
        this.veneno = pVeneno;
    }

    /**
     * @return the claAraneae
     */
    public String getClaAraneae() {
        return claAraneae;
    }

    /**
     * @param claAraneae the claAraneae to set
     */
    public void setClaAraneae(String claAraneae) {
        this.claAraneae = claAraneae;
    }

    /**
     * @return the veneno
     */
    public boolean isVeneno() {
        return veneno;
    }

    /**
     * @param veneno the veneno to set
     */
    public void setVeneno(boolean veneno) {
        this.veneno = veneno;
    }
    
}
