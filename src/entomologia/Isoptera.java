/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Pablo08
 */
public class Isoptera extends Entomologia{
    private  String tipTermita;
    private  String cicBiotico;
    
    
    public Isoptera(String pClase, String pReino, String pFilo, String pOrden, String pSubOrden, String pTipTermita, String pCicBiotico) {
        super(pClase, pReino, pFilo, pOrden, pSubOrden);
        this.tipTermita = pTipTermita;
        this.cicBiotico = pCicBiotico;
    }

    /**
     * @return the tipTermita
     */
    public String getTipTermita() {
        return tipTermita;
    }

    /**
     * @param tipTermita the tipTermita to set
     */
    public void setTipTermita(String tipTermita) {
        this.tipTermita = tipTermita;
    }

    /**
     * @return the cicBiotico
     */
    public String getCicBiotico() {
        return cicBiotico;
    }

    /**
     * @param cicBiotico the cicBiotico to set
     */
    public void setCicBiotico(String cicBiotico) {
        this.cicBiotico = cicBiotico;
    }
    
}
