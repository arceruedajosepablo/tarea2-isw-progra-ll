/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entomologia;

/**
 *
 * @author Pablo08
 */
public class Entomologia {


    
//ATRIBUTOS
   
    private String clase;
    private String reino;
    private String filo;
    private String orden;
    private String subOrden;

    public Entomologia(String pClase,String pReino, String pFilo, String pOrden, String pSubOrden ) {
        this.clase = pClase;
        this.reino = pReino;
        this.filo = pFilo;
        this.orden = pOrden;
        this.subOrden = pSubOrden;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the reino
     */
    public String getReino() {
        return reino;
    }

    /**
     * @param reino the reino to set
     */
    public void setReino(String reino) {
        this.reino = reino;
    }

    /**
     * @return the filo
     */
    public String getFilo() {
        return filo;
    }

    /**
     * @param filo the filo to set
     */
    public void setFilo(String filo) {
        this.filo = filo;
    }

    /**
     * @return the orden
     */
    public String getOrden() {
        return orden;
    }

    /**
     * @param orden the orden to set
     */
    public void setOrden(String orden) {
        this.orden = orden;
    }

    /**
     * @return the subOrden
     */
    public String getSubOrden() {
        return subOrden;
    }

    /**
     * @param subOrden the subOrden to set
     */
    public void setSubOrden(String subOrden) {
        this.subOrden = subOrden;
    }
}
